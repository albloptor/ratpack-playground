package ratpack.module;

import com.google.inject.AbstractModule;
import ratpack.service.BootstrapService;

public final class ServiceModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(BootstrapService.class);
    }
}
