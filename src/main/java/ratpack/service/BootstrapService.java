package ratpack.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;

@Singleton
public final class BootstrapService implements Service {

    private static final Logger log = LoggerFactory.getLogger(BootstrapService.class);

    @Override
    public void onStart(StartEvent event) {
        log.info("BootstrapService onStart()...");
    }

    @Override
    public void onStop(StopEvent event) {
        log.info("BootstrapService onStop()...");
    }
}

